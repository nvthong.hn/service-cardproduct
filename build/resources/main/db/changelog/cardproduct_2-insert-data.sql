--liquibase formatted sql
--changeset microservice_class:20230725_2

INSERT INTO CARD_PRODUCT
(id, BIN, PRODUCT_TYPE, PRODUCT_CODE, PRODUCT_NAME, PRODUCT_DESC, updated_at)
VALUES ('6364bf5d-fa89-4ab3-b274-1da8cdb785b3', '970436', 'D', '001', 'Debit card', 'Test debit card', '2023-07-25 11:34:53.629'),
       ('6364bf5d-fa89-4ab3-b274-1da8cdb785b5', '475320', 'C', '002', 'Credit card', 'Test credit card', '2023-07-25 11:35:53.629');