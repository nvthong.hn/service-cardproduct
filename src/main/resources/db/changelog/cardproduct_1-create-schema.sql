--liquibase formatted sql
--changeset microservice_class:20230725_1

CREATE
EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE CARD_PRODUCT
(
    ID         uuid PRIMARY KEY default uuid_generate_v4(),
    BIN    varchar(255),
    PRODUCT_TYPE    varchar(255),
    PRODUCT_CODE    varchar(255),
    PRODUCT_NAME    varchar(255),
    PRODUCT_DESC    varchar(255),
    UPDATED_AT timestamp
);