package com.vietcombank.service.model;

import com.vietcombank.service.entity.CardProductEntity;

public class GetProductByIdRes {
    private String responseID;
    private int resCode;
    private String resMessage;
    private CardProductEntity itemProduct;

    public CardProductEntity getItemProduct() {
        return itemProduct;
    }

    public void setItemProduct(CardProductEntity itemProduct) {
        this.itemProduct = itemProduct;
    }

    public int getResCode() {
        return resCode;
    }

    public void setResCode(int resCode) {
        this.resCode = resCode;
    }

    public String getResMessage() {
        return resMessage;
    }

    public void setResMessage(String resMessage) {
        this.resMessage = resMessage;
    }

    public String getResponseID() {
        return responseID;
    }

    public void setResponseID(String responseID) {
        this.responseID = responseID;
    }
}
