package com.vietcombank.service.model;

import com.vietcombank.service.entity.CardProductEntity;

import java.util.List;

public class GetAllProductRes {
    private String responseID;
    private int resCode;
    private String resMessage;
    private List<CardProductEntity> listProduct;

    public int getResCode() {
        return resCode;
    }

    public void setResCode(int resCode) {
        this.resCode = resCode;
    }

    public String getResMessage() {
        return resMessage;
    }

    public void setResMessage(String resMessage) {
        this.resMessage = resMessage;
    }

    public List<CardProductEntity> getListProduct() {
        return listProduct;
    }

    public void setListProduct(List<CardProductEntity> listProduct) {
        this.listProduct = listProduct;
    }

    public String getResponseID() {
        return responseID;
    }

    public void setResponseID(String responseID) {
        this.responseID = responseID;
    }
}
