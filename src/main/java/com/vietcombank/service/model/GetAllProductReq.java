package com.vietcombank.service.model;

public class GetAllProductReq {
    private String requestID;

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }
}
