package com.vietcombank.service.repository;

import com.vietcombank.service.entity.CardProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface CardProductEntityRepository extends JpaRepository<CardProductEntity, UUID> {
    public List<CardProductEntity> findByProductCode(String productCode);
}