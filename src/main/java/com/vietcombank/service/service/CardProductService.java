package com.vietcombank.service.service;

import com.vietcombank.service.entity.CardProductEntity;
import com.vietcombank.service.model.*;
import com.vietcombank.service.repository.CardProductEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
public class CardProductService {
    @Autowired
    private CardProductEntityRepository cardProductEntityRepository;

    public List<CardProductEntity> retrieveAll() {
        return cardProductEntityRepository.findAll();
    }
    public String insertCardProduct(InsertProductReq req)
    {
        CardProductEntity entity = new CardProductEntity();
        entity.setId(UUID.randomUUID());
        entity.setBin(req.getBin());
        entity.setProductCode(req.getProductCode());
        entity.setProductName(req.getProductName());
        entity.setProductType(req.getProductType());
        entity.setProductDesc(req.getProductDesc());
        entity.setUpdatedAt(Instant.now());
        cardProductEntityRepository.save(entity);
        return entity.getId().toString();
    }
    public boolean deleteCardProduct(DeleteProductByIdReq req)
    {
        try {
            UUID uid = UUID.fromString(req.getId());
            cardProductEntityRepository.deleteById(uid);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }
    public boolean updateCardProduct(UpdateProductReq req)
    {
        try {
            UUID uid = UUID.fromString(req.getId());
            CardProductEntity entity = cardProductEntityRepository.findById(uid).get();
            if (entity == null)
            {
                return false;
            }
            if (req.getBin() != null && req.getBin() != "")
            {
                entity.setBin(req.getBin());
            }
            if (req.getProductDesc() != null && req.getProductDesc() != "")
            {
                entity.setProductDesc(req.getProductDesc());
            }
            if (req.getProductCode() != null && req.getProductCode() != "")
            {
                entity.setProductCode(req.getProductCode());
            }
            if (req.getProductName() != null && req.getProductName() != "")
            {
                entity.setProductName(req.getProductName());
            }
            if (req.getProductType() != null && req.getProductType() != "")
            {
                entity.setProductType(req.getProductType());
            }
            entity.setUpdatedAt(Instant.now());
            cardProductEntityRepository.save(entity);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }
    public CardProductEntity getProductById(GetProductByIdReq req)
    {
        try {
            UUID uid = UUID.fromString(req.getId());
            CardProductEntity entity = cardProductEntityRepository.findById(uid).get();
            if (entity == null)
            {
                return null;
            }
            return entity;
        }
        catch (Exception ex)
        {
            return null;
        }
    }
}
