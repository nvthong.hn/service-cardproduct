package com.vietcombank.service.controller;

import com.vietcombank.service.entity.CardProductEntity;
import com.vietcombank.service.model.*;
import com.vietcombank.service.service.CardProductService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/restapi")
public class CardProductController {
    @Autowired
    private CardProductService cardProductService;

    @GetMapping(value = "/v1/products", produces = "application/json")
    @Operation(summary = "Retrieve All Product")
    public ResponseEntity<?> retrieveProducts() {
        log.info("retrieve all products");
        return ResponseEntity.ok(cardProductService.retrieveAll());
    }
    @PostMapping(value = "/v1/insertproduct")
    @Operation(summary = "Insert new product")
    public InsertProductRes insertProduct(@RequestBody InsertProductReq req) {
        String msgID = req.getRequestID();
        InsertProductRes res = new InsertProductRes();
        res.setResponseID(msgID);
        log.info(msgID + "insert new item");
        try
        {
            String id = cardProductService.insertCardProduct(req);
            if (id != null)
            {
                res.setResCode(0);
                res.setResMessage("Success");
                res.setIdProduct(id);
            }
            else
            {
                res.setResCode(1);
                res.setResMessage("Failed");
            }
        }
        catch (Exception ex)
        {
            log.error(msgID + "exception error" + ex.getMessage());
            res.setResCode(99);
            res.setResMessage("exception " + ex.getMessage());
            return res;
        }
        return res;
    }
    @PostMapping(value = "/v1/getAllProduct")
    @Operation(summary = "Get All Product")
    public GetAllProductRes getAllProduct(@RequestBody GetAllProductReq req) {
        String msgID = req.getRequestID();
        GetAllProductRes res = new GetAllProductRes();
        res.setResponseID(msgID);
        log.info(msgID + "get all product");
        try
        {
            List<CardProductEntity> listValue = cardProductService.retrieveAll();
            if (listValue != null)
            {
                res.setResCode(0);
                res.setResMessage("Success");
                res.setListProduct(listValue);
            }
            else
            {
                res.setResCode(1);
                res.setResMessage("Failed");
            }
        }
        catch (Exception ex)
        {
            log.error(msgID + "exception " + ex.getMessage());
            res.setResCode(99);
            res.setResMessage("exception " + ex.getMessage());
            return res;
        }
        return res;
    }
    @PostMapping(value = "/v1/getProductById")
    @Operation(summary = "Get Product By Id")
    public GetProductByIdRes getProductById(@RequestBody GetProductByIdReq req) {
        String msgID = req.getRequestID();
        GetProductByIdRes res = new GetProductByIdRes();
        res.setResponseID(msgID);
        log.info(msgID + "Get Product By Id");
        try
        {
            CardProductEntity values = cardProductService.getProductById(req);
            if (values != null)
            {
                res.setResCode(0);
                res.setResMessage("Success");
                res.setItemProduct(values);
            }
            else
            {
                res.setResCode(1);
                res.setResMessage("Failed");
            }
        }
        catch (Exception ex)
        {
            log.error(msgID + "exception " + ex.getMessage());
            res.setResCode(99);
            res.setResMessage("exception " + ex.getMessage());
            return res;
        }
        return res;
    }
    @PostMapping(value = "/v1/updateProductById")
    @Operation(summary = "Update Product By Id")
    public UpdateProductRes updateProductById(@RequestBody UpdateProductReq req) {
        String msgID = req.getRequestID();
        UpdateProductRes res = new UpdateProductRes();
        res.setResponseID(msgID);
        log.info(msgID + "Update Product By Id");
        try
        {
            Boolean values = cardProductService.updateCardProduct(req);
            if (values)
            {
                res.setResCode(0);
                res.setResMessage("Success");
            }
            else
            {
                res.setResCode(1);
                res.setResMessage("Failed");
            }
        }
        catch (Exception ex)
        {
            log.error(msgID + "exception " + ex.getMessage());
            res.setResCode(99);
            res.setResMessage("exception " + ex.getMessage());
            return res;
        }
        return res;
    }
    @PostMapping(value = "/v1/deleteProductById")
    @Operation(summary = "Delete Product By Id")
    public DeleteProductByIdRes deleteProductById(@RequestBody DeleteProductByIdReq req) {
        String msgID = req.getRequestID();
        DeleteProductByIdRes res = new DeleteProductByIdRes();
        res.setResponseID(msgID);
        log.info(msgID + "eleteProductById Product By Id ");
        try
        {
            Boolean values = cardProductService.deleteCardProduct(req);
            if (values)
            {
                res.setResCode(0);
                res.setResMessage("Success");
            }
            else
            {
                res.setResCode(1);
                res.setResMessage("Failed");
            }
        }
        catch (Exception ex)
        {
            log.error(msgID + "exception " + ex.getMessage());
            res.setResCode(99);
            res.setResMessage("exception " + ex.getMessage());
            return res;
        }
        return res;
    }
}
